package util;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public class Constants {
    public static final String SITES_COUNT = "sites_count";
    public static final String SITE_PROP_PREFIX = "site";
    public static final String SITE_PROP_URl = ".url";
    public static final String SITE_PROP_CLASS_NAME = ".className";
    public static final String SITE_PROP_NAME = ".name";
}
