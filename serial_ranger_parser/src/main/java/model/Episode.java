package model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by Ольга on 02.06.2014.
 */
@Entity
public class Episode {

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    private Long id;

    private Integer lastEpisode;

    @ManyToOne
    @JoinColumn(name = "film_id")
    private Film film;

    public Episode() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLastEpisode() {
        return lastEpisode;
    }

    public void setLastEpisode(Integer lastEpisode) {
        this.lastEpisode = lastEpisode;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }
}
