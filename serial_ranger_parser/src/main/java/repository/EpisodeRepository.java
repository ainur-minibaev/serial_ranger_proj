package repository;

import model.Episode;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Ольга on 02.06.2014.
 */
public interface EpisodeRepository extends CrudRepository<Episode, Long> {
    Episode findEpisodeByFilmId(Long filmId);
}
