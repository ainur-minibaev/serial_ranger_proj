package repository;

import model.Film;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public interface FilmRepository extends CrudRepository<Film, Long> {
    Film findFilmByNameAndSiteLink(String name, String siteLink);
}
