package parsers.impl;

import it.sauronsoftware.feed4j.FeedParser;
import it.sauronsoftware.feed4j.bean.Feed;
import it.sauronsoftware.feed4j.bean.FeedItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import parsers.Parser;
import service.FilmService;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ainurminibaev on 02.06.14.
 */
@Service("anidub")
public class AnidubParser implements Parser {
    String link;
    private List<String> containedFilms;

    @Autowired
    FilmService filmService;

    public AnidubParser() {
        containedFilms = new ArrayList<String>();
    }

    @Override
    public void updateInfo(String link) {
        this.link = link;
        try {
            URL url = new URL(link);
            Feed feed = FeedParser.parse(url);
            int count = feed.getItemCount();
            for (int i = 0; i < count; i++) {
                FeedItem item = feed.getItem(i);
                String title = item.getTitle();
                String filmName = getFilmName(title);
                Integer episodeNum = getEpisode(title);
                filmService.saveOrUpdateEpisode(episodeNum, filmName, link.substring(0, link.lastIndexOf('/')));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Integer getEpisode(String title) {
        int num = title.indexOf('[') + 1;
        StringBuilder builder = new StringBuilder();
        while (title.charAt(num) >= '0' && title.charAt(num) <= '9') {
            builder.append(title.charAt(num));
            num++;
        }
        try {
            Integer episodeNum = Integer.valueOf(builder.toString());
            return episodeNum;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getFilmName(String title) {
        String name = title.substring(0, title.indexOf('[') - 1);
        return name;
    }
}
