package main;

import config.CoreConfig;
import config.DataSourceConfig;
import config.PersistenceConfig;
import it.sauronsoftware.feed4j.FeedIOException;
import it.sauronsoftware.feed4j.FeedXMLParseException;
import it.sauronsoftware.feed4j.UnsupportedFeedException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import parsers.Parser;
import util.Constants;

import java.net.MalformedURLException;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public class MainParser {


    public static void main(String[] args) throws MalformedURLException, FeedIOException, UnsupportedFeedException, FeedXMLParseException, ClassNotFoundException {
        AnnotationConfigApplicationContext ctx =
                new AnnotationConfigApplicationContext();
        ctx.register(DataSourceConfig.class, PersistenceConfig.class, CoreConfig.class);
        ctx.refresh();
        ConfigurableEnvironment environment = ctx.getEnvironment();
        Integer sitesCount = Integer.valueOf(environment.getProperty(Constants.SITES_COUNT));
        for (int i = 1; i <= sitesCount; i++) {
            String siteProp = Constants.SITE_PROP_PREFIX + i;
            String siteUrl = environment.getProperty(siteProp + Constants.SITE_PROP_URl);
            String siteName = environment.getProperty(siteProp + Constants.SITE_PROP_NAME);
            String parserClassName = environment.getProperty(siteProp + Constants.SITE_PROP_CLASS_NAME);
            Class siteClass = Class.forName(parserClassName);
            Parser siteParser = ctx.getBean(siteName, Parser.class);
            siteParser.updateInfo(siteUrl);
        }
        System.out.println("Success");
    }
}
