package config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(value = {"service", "parsers"})
@PropertySource("classpath:site_list.properties")
public class CoreConfig {
}
