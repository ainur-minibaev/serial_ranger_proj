package service.impl;

import model.Episode;
import model.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.EpisodeRepository;
import repository.FilmRepository;
import service.FilmService;


/**
 * Created by ainurminibaev on 02.06.14.
 */
@Service
public class FilmServiceImpl implements FilmService {
    @Autowired
    FilmRepository filmRepository;

    @Autowired
    EpisodeRepository episodeRepository;

    @Override
    public Film findFilm(String name, String siteLink) {
        return filmRepository.findFilmByNameAndSiteLink(name, siteLink);
    }

    @Override
    public Film getOrCreate(String filmName, String siteLink) {
        Film film = findFilm(filmName, siteLink);
        if (film == null) {
            film = new Film();
            film.setName(filmName);
            //TODO add images and descr
            film.setSiteLink(siteLink);
            filmRepository.save(film);
        }
        return film;
    }

    @Override
    public void saveOrUpdateEpisode(Integer episodeNum, String filmName, String siteLink) {
        Film film = getOrCreate(filmName, siteLink);
        Episode episode = episodeRepository.findEpisodeByFilmId(film.getId());
        if (episode == null) {
            episode = new Episode();
            episode.setLastEpisode(episodeNum);
            episode.setFilm(film);
            episodeRepository.save(episode);
        }
        Integer last_episode = episode.getLastEpisode();
        if (last_episode == null || last_episode < episodeNum) {
            episode.setLastEpisode(episodeNum);
            episodeRepository.save(episode);
        }
    }


}
