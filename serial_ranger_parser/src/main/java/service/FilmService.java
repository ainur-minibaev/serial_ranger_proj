package service;

import model.Episode;
import model.Film;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public interface FilmService {
    Film findFilm(String name, String link);

    Film getOrCreate(String filmName, String link);



    void saveOrUpdateEpisode(Integer episodeNum, String filmName,String siteLink);
}
