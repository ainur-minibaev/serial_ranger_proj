package auth;

import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import service.UserService;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by ainurminibaev on 12.05.14.
 */
@Component
public class AuthProvider implements AuthenticationProvider {

    @Autowired
    UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String password = authentication.getCredentials().toString();
        String email = authentication.getName();
        User user = userService.findUser(email);
        if (user == null) {
            throw new UsernameNotFoundException("User Not Found");
        }
        String hashedPassword = PasswordHelper.encrypt(password, user.getSalt());
        if (user.getPassword().equals(hashedPassword)) {
            return setAuthentication(user.getId(), user, Arrays.asList(new SimpleGrantedAuthority("USER")));
        } else {
            throw new BadCredentialsException("Bad user password");
        }
    }

    private Authentication setAuthentication(Long id, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        Authentication authToken =
                new UsernamePasswordAuthenticationToken(
                        id,
                        credentials,
                        Arrays.asList(new SimpleGrantedAuthority("USER"))
                );
        SecurityContextHolder.getContext().setAuthentication(authToken);
        return authToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
  /*  @Autowired
    CredentialsService credentialsService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String password = authentication.getCredentials().toString();
        String email = authentication.getName();
        Company company = credentialsService.findCompany(email);
        User user = null;
        //NO common class for credentials, so a lot of repeating code
        if (company == null) {
            user = credentialsService.findUser(email);
            if (user == null) {
                throw new UsernameNotFoundException("User Not Found");
            }
            String hashedPassword = PasswordHelper.encrypt(password, user.getSalt());
            if (user.getPassword().equals(hashedPassword)) {
                return setAuthentication(user.getId(), user, Arrays.asList(new SimpleGrantedAuthority("USER")));
            } else {
                throw new BadCredentialsException("Bad user password");
            }
        }
        String hashedPass = PasswordHelper.encrypt(password, company.getSalt());
        if (company.getPassword().equals(hashedPass)) {
            return setAuthentication(company.getId(), company, Arrays.asList(new SimpleGrantedAuthority("COMPANY")));
        } else {
            throw new BadCredentialsException("Bad Credentials: bad pass");
        }

    }


    private Authentication setAuthentication(Long id, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        Authentication authToken =
                new UsernamePasswordAuthenticationToken(
                        id,
                        credentials,
                        Arrays.asList(new SimpleGrantedAuthority("USER"))
                );
        SecurityContextHolder.getContext().setAuthentication(authToken);
        return authToken;
    }


    @Override
    public boolean supports(Class<?> aClass) {
//        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(aClass));
        return true;
    }*/
}
