package auth;

import forms.RegForm;
import model.User;

import java.util.UUID;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public class Mappers {

    public static User regMapper(RegForm regForm) {
        String salt = UUID.randomUUID().toString();
        User user = new User();
        user.setLogin(regForm.getLogin());
        user.setEmail(regForm.getEmail());
        user.setSalt(salt);
        user.setPassword(PasswordHelper.encrypt(regForm.getPassword(), salt));
        return user;
    }
}
