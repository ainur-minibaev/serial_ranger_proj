package auth;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by ainurminibaev on 13.05.14.
 */
public class PasswordHelper {


    private static String getSaltedPass(String password, String salt) {
        return password + salt;
    }

    public static String encrypt(String unencrypted, String salt) {
        return DigestUtils.md5Hex(getSaltedPass(unencrypted, salt));
    }

}
