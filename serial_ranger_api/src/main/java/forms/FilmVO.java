package forms;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public class FilmVO {
    String label;
    Long id;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
