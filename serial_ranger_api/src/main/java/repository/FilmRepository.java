package repository;

import model.Film;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public interface FilmRepository extends CrudRepository<Film, Long> {

    List<Film> findByNameContaining(String term);
}
