package repository;

import model.Episode;
import model.Film;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Ольга on 02.06.2014.
 */
public interface EpisodeRepository extends CrudRepository<Episode, Long> {
    List<Episode> findByFilmIn(List<Film> films);

    Episode findOneByFilmAndLastEpisode(Film film, Integer lastEpisode);
}
