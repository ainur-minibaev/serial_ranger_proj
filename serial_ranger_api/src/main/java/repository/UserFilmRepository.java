package repository;

import model.Film;
import model.User;
import model.UserFilm;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Ольга on 02.06.2014.
 */
public interface UserFilmRepository extends CrudRepository<UserFilm, Long> {
    List<UserFilm> findByUser(User user);

    UserFilm findOneByUserAndFilm(User user, Film film);
}
