package controller;

import model.Episode;
import model.User;
import model.UserFilm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import service.EpisodeService;
import service.FilmService;
import service.UserFilmService;
import service.UserService;

import java.util.List;

/**
 * Created by ainurminibaev on 02.06.14.
 */
@Controller
public class UserPageController {
    @Autowired
    UserService userService;

    @Autowired
    FilmService filmService;

    @Autowired
    UserFilmService userFilmService;
    @Autowired
    EpisodeService episodeService;


    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String renderUserPage(ModelMap map) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.findById((Long) principal);
        map.addAttribute("user", user);
        List<Episode> unwatchedList = episodeService.getUnwatchedList(user);
        List<UserFilm> userWatchedFilms = userFilmService.getUserWatchedFilms(user, unwatchedList);
        map.addAttribute("watchedFilms", userWatchedFilms);
        map.addAttribute("unwatchedFilms", unwatchedList);
        return "content";
    }

    @RequestMapping(value = "/film/episode", method = RequestMethod.GET)
    @ResponseBody
    public boolean markEpisodeAsWatched(@RequestParam Long filmId, @RequestParam Long episode) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.findById((Long) principal);
        userFilmService.markFilmAsWatched(user, filmId, episode);
        return true;
    }

    @RequestMapping(value = "/film/add", method = RequestMethod.GET)
    @ResponseBody
    public boolean addFilm(@RequestParam Long filmId) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.findById((Long) principal);
        boolean b = userFilmService.addFilm(user, filmId);
        return true;
    }
}
