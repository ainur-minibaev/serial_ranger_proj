package controller;

import auth.Mappers;
import forms.RegForm;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import service.UserService;

/**
 * Created by Ольга on 02.06.2014.
 */
@Controller
public class RegistrationController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/reg", method = RequestMethod.GET)
    public String renderRegPage(ModelMap map) {
        map.addAttribute("regForm", new RegForm());
        return "register";
    }

    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    public String regUser(@ModelAttribute("regForm") RegForm form, BindingResult result, ModelMap map) {
        User user = userService.findUser(form.getEmail());
        if (user != null) {
            return "register";
        }
        User newUser = Mappers.regMapper(form);
        userService.reg(newUser);
        return "redirect:/";
    }

}
