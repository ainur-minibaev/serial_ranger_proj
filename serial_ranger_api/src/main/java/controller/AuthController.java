package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ainurminibaev on 02.06.14.
 */
@Controller
public class AuthController {


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String renderLogin() {
        return "index";
    }
}
