package controller;

import forms.FilmVO;
import model.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import service.FilmService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ainurminibaev on 02.06.14.
 */
@Controller
public class UtilController {
    @Autowired
    FilmService filmService;


    @RequestMapping(value = "/film/search", method = RequestMethod.GET)
    @ResponseBody
    public List<FilmVO> getFilmByTerm(@RequestParam String term) {
        if (term == null && term.trim().length() == 0) {
            return Collections.<FilmVO>emptyList();
        }
        List<Film> byTerm = filmService.findByTerm(term);
        List<FilmVO> filmVOs = new ArrayList<FilmVO>(byTerm.size());
        for (Film film : byTerm) {
            FilmVO filmVO = new FilmVO();
            filmVO.setLabel(film.getName());
            filmVO.setId(film.getId());
            filmVOs.add(filmVO);
        }
        return filmVOs;
    }
}
