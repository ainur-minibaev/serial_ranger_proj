package service;

import model.Episode;
import model.User;

import java.util.List;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public interface EpisodeService {
    List<Episode> getUnwatchedList(User user);

}
