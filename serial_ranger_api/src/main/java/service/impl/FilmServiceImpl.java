package service.impl;

import model.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.FilmRepository;
import service.FilmService;

import java.util.List;

/**
 * Created by ainurminibaev on 02.06.14.
 */
@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    FilmRepository filmRepository;

    @Override
    public List<Film> findByTerm(String term) {
        return filmRepository.findByNameContaining(term);
    }
}
