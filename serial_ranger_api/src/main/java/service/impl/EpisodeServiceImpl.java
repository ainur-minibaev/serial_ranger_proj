package service.impl;

import model.Episode;
import model.Film;
import model.User;
import model.UserFilm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.EpisodeRepository;
import repository.UserFilmRepository;
import service.EpisodeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ainurminibaev on 02.06.14.
 */
@Service
public class EpisodeServiceImpl implements EpisodeService {
    @Autowired
    EpisodeRepository episodeRepository;
    @Autowired
    UserFilmRepository userFilmRepository;

    @Override
    public List<Episode> getUnwatchedList(User user) {
        List<Film> userFilms = getUserFilms(user);
        if (userFilms.size() == 0) {
            return Collections.<Episode>emptyList();
        }
        List<Episode> episodes = episodeRepository.findByFilmIn(userFilms);
        List<Episode> newEpisodes = findNewEpisodes(episodes, user);
        return newEpisodes;
    }

    private List<Episode> findNewEpisodes(List<Episode> episodes, User user) {
        List<Episode> newEpisodes = new LinkedList<Episode>();
        List<UserFilm> byUser = userFilmRepository.findByUser(user);
        for (UserFilm userFilm : byUser) {
            for (Episode episode : episodes) {
                if (episode.getFilm().getId().equals(userFilm.getFilm().getId())) {
                    Episode episode1 = userFilm.getEpisode();
                    Integer lastWatchedEpisode = null;
                    if (episode1 != null) {
                        lastWatchedEpisode = episode1.getLastEpisode();
                    }
                    Integer newEpisodeId = episode.getLastEpisode();
                    if (lastWatchedEpisode == null || newEpisodeId > lastWatchedEpisode) {
                        newEpisodes.add(episode);
                    }
                }
            }
        }
        return newEpisodes;
    }

    private List<Film> getUserFilms(User user) {
        List<UserFilm> byUser = userFilmRepository.findByUser(user);
        List<Film> films = new ArrayList<Film>(byUser.size());
        for (UserFilm userFilm : byUser) {
            Film e = new Film();
            films.add(userFilm.getFilm());
        }
        return films;
    }


}
