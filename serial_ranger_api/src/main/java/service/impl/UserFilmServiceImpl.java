package service.impl;

import model.Episode;
import model.Film;
import model.User;
import model.UserFilm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.EpisodeRepository;
import repository.FilmRepository;
import repository.UserFilmRepository;
import service.UserFilmService;

import java.util.List;

/**
 * Created by ainurminibaev on 02.06.14.
 */
@Service
public class UserFilmServiceImpl implements UserFilmService {
    @Autowired
    UserFilmRepository userFilmRepository;
    @Autowired
    FilmRepository filmRepository;
    @Autowired
    EpisodeRepository episodeRepository;

    @Override
    public List<UserFilm> getUserWatchedFilms(User user, List<Episode> unwatchedFilms) {
        List<UserFilm> byUser = userFilmRepository.findByUser(user);
        for (int i = 0; i < byUser.size(); i++) {
            UserFilm userFilm = byUser.get(i);
            for (Episode episode : unwatchedFilms) {
                if (episode.getFilm().getId().equals(userFilm.getFilm().getId())) {
                    try {
                        byUser.remove(i);
                    } catch (Exception e) {

                    }
                }
            }
        }
        return byUser;
    }

    @Override
    public boolean markFilmAsWatched(User user, Long filmId, Long episode) {
        Film one = filmRepository.findOne(filmId);
        Episode episodeObj = episodeRepository.findOneByFilmAndLastEpisode(one, episode.intValue());
        if (one == null || episodeObj == null) {
            return false;
        }
        UserFilm userFilm = userFilmRepository.findOneByUserAndFilm(user, one);
        userFilm.setEpisode(episodeObj);
        userFilmRepository.save(userFilm);
        return true;
    }

    @Override
    public boolean addFilm(User user, Long filmId) {
        Film one = filmRepository.findOne(filmId);
        if (one == null) {
            return false;
        }
        UserFilm userFilm = new UserFilm();
        userFilm.setUser(user);
        userFilm.setFilm(one);
        userFilmRepository.save(userFilm);
        return false;
    }
}
