package service;

import model.Episode;
import model.User;
import model.UserFilm;

import java.util.List;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public interface UserFilmService {
    List<UserFilm> getUserWatchedFilms(User user, List<Episode> unwatchedFilms);

    boolean markFilmAsWatched(User user, Long filmId, Long episode);


    boolean addFilm(User user,Long filmId);
}
