package service;

import model.User;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public interface UserService {

    User findUser(String email);

    User reg(User user);

    User findById(Long id);

}
