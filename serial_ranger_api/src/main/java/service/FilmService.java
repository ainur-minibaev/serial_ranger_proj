package service;

import model.Film;

import java.util.List;

/**
 * Created by ainurminibaev on 02.06.14.
 */
public interface FilmService {

    List<Film> findByTerm(String term);
}
