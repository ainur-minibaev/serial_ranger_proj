<#include "../templates/mainTemplate.ftl">
<#assign form=JspTaglibs["http://www.springframework.org/tags/form"]>
<#assign spring=JspTaglibs["http://www.springframework.org/tags"]>
<#macro m_body>
<section>
    <@form.form commandName="regForm" id="theForm" cssClass="simform"  autocomplete="off" action="/reg" method="POST">
        <div class="simform-inner">
            <ol class="questions">
                <li>
                    <span><label for="login">Ваш логин:</label></span>
                    <@form.input path="login" id="q1" autocomplete="off"/>
                </li>
                <li>
                    <span><label for="email">Ваш email:</label></span>
                    <@form.input path="email" id="q2" autocomplete="off"/>
                </li>
                <li>
                    <span><label for="password">Ваш пароль</label></span>
                    <@form.password path="password" id="q3" autocomplete="off"/>
                </li>
            </ol>
            <!-- /questions -->
            <button class="submit" type="submit">Отослать ответы</button>
            <div class="controls">
                <button class="next"></button>
                <div class="progress"></div>
							<span class="number">
								<span class="number-current"></span>
								<span class="number-total"></span>
							</span>
                <span class="error-message"></span>
            </div>
            <!-- / controls -->
        </div>
        <!-- /simform-inner -->
        <span class="final-message"></span>
    </@form.form>
    <!-- /simform -->
</section>
</#macro>
<#macro m_customEnding>
<script src="/resources/js/classie.js"></script>
<script src="/resources/js/stepsForm.js"></script>
<script>
    var theForm = document.getElementById('theForm');

    new stepsForm(theForm, {
        onSubmit: function (form) {
            // hide form
            classie.addClass(theForm.querySelector('.simform-inner'), 'hide');
            form.submit()
        }
    });
</script>
</#macro>
<@main
customScripts=[
"/resources/js/modernizr.custom.js"
]
customStyles=[
"/resources/css/normalize.css",
"/resources/css/component.css"
]
offDefaultStyles=true
/>