<#include "../templates/mainTemplate.ftl">
<#macro m_body>
<input id="newfilmId" type="hidden"/>

<header>
    <h1 class="retroshadow">Serial Ranger</h1>
</header>
<div class="container_serial">
    <form>
        <input type="text" class="input_serial" id="serial_search"/>
        <button type="button" class="add_serial" id="new_serial">Отслеживать новый сериал</button>
    </form>

    <p>
        <#list unwatchedFilms as episode>
            <article id="article${episode.film.id}" class="col-md-3 episode new_ep">
                <img src="http://static2.anidub.com/tracker/poster/4dc7d40cd1.jpg"/>
                <header>
                    <a href="" class="title">Сломанный Меч / Break Blade TV Сломанный Меч / Break Blade TV</a>

                    <p>Новые серии: <span class="ep">${episode.lastEpisode!}</span>
                        <button id="button${episode.film.id}" class="typicons-tick confirm"
                                value="${episode.film.id}" data-filmep="22"></button>
                        <input type="hidden" id="inv${episode.film.id}"
                               value="${episode.lastEpisode!}"/>
                    </p>
                </header>
                <div class="clean"></div>
            </article>
        </#list>
    </p>
</div>
</#macro>

<@main
customScripts=["/resources/js/app/seach-bar.js"]
/>
