<#include "../templates/mainTemplate.ftl">
<#macro m_body>
<div class="sub-container">
    <div class="signin">
        <h1 class='retroshadow'>Serial <br/>Ranger</h1>
        <!--<h1>Serial <br/>Ranger</h1>-->
        <form action="/login" method="POST" th:action="@{/login}">
            <div class="wrapper">
                <label for="username"><span class="typicons-user"></span></label>
                <input type="text" name="username" id="username" placeholder="Email адрес" autocomplete="off"/>
            </div>
            <div class="wrapper">
                <label for="password"><span class="typicons-lock"></span></label>
                <input type="password" name="password" id="password" placeholder="Пароль" autocomplete="off"/>
            </div>
            <input type="submit" value="Войти"/>
        </form>
    </div>
    <footer>
        <p><a href="/reg">Зарегаться</a></p>
    </footer>
    <!--<p class="forget"><a href="#">Забыли пароль?</a></p>-->
</div>
</#macro>
<@main/>