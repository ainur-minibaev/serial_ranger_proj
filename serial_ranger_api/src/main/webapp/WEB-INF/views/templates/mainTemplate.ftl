<#macro main customScripts=[] customStyles=[] offDefaultStyles=false>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Serial Ranger</title>
    <#if !offDefaultStyles>
        <link href="/resources/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/css/index_style.css" rel="stylesheet" type="text/css"/>
        <link href='http://fonts.googleapis.com/css?family=Nunito:300' rel='stylesheet' type='text/css'/>
        <link href="/resources/css/jquery-ui-1.10.4.custom.css" rel="stylesheet"/>
    </#if>
    <#list customStyles as style>
        <link href="${style}" rel="stylesheet"/>
    </#list>

    <script src="/resources/js/jquery-2.1.0.min.js"></script>
    <script src="/resources/js/jquery-ui-1.10.4.custom.js"></script>
    <#list customScripts as script>
        <script src="${script}"></script>
    </#list>
<body>
<div class="container">
    <#if m_body?? && m_body?is_directive>
                    <@m_body/>
                <#else>
    ${m_body}
    </#if>
</div>

    <#if m_customEnding??>
        <@m_customEnding/>
    </#if>
</body>
</#macro>