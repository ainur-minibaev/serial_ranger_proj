$(function () {
    $("#serial_search").autocomplete({
        source: "/film/search",
        select: function (event, ui) {
            console.log(ui.item ?
                "Selected: " + ui.item.value + " aka " + ui.item.id :
                "Nothing selected, input was " + this.value);
            $("#newfilmId").val(ui.item.id);
            alert(ui.item.id);
        }
    });
});
$(function () {
    $(".confirm").click(function () {
        $.ajax({
            type: "GET",
            url: "/film/episode",
            data: {filmId: this.value, episode: $("#inv" + this.value).val()}
        }).done(function (msg) {
            $("#article" + this.value).removeClass("new_ep");
            $("#button" + this.value).hide();
        });
    });

    $("#new_serial").click(function () {
        $.ajax({
            type: "GET",
            url: "/film/add",
            data: {filmId: $("#newfilmId").val()}
        }).success(function (msg) {
            alert("New Film added");
        });
        return false;
    });
});