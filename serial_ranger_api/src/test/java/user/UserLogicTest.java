package user;

import config.CoreConfig;
import config.DataSourceTestConfig;
import config.PersistenceConfig;
import model.Episode;
import model.Film;
import model.User;
import model.UserFilm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import repository.EpisodeRepository;
import repository.FilmRepository;
import repository.UserFilmRepository;
import repository.UserRepository;
import service.EpisodeService;
import service.FilmService;
import service.UserService;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by ainurminibaev on 02.06.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceTestConfig.class, PersistenceConfig.class, CoreConfig.class})
public class UserLogicTest {

    @Autowired
    UserService userService;
    @Autowired
    FilmService filmService;
    @Autowired
    EpisodeService episodeService;
    @Autowired
    FilmRepository filmRepository;
    @Autowired
    EpisodeRepository episodeRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserFilmRepository userFilmRepository;

    @Test
    public void test() {
        Film film = new Film();
        film.setName("Name of Thr");
        filmRepository.save(film);
        Episode episode = new Episode();
        episode.setLastEpisode(10);
        episode.setFilm(film);

        Episode episode1 = new Episode();
        episode1.setLastEpisode(9);
        episode1.setFilm(film);
        episodeRepository.save(episode);
        episodeRepository.save(episode1);
        UserFilm userFilm = new UserFilm();
        userFilm.setFilm(film);
        userFilm.setEpisode(episode1);
        assertTrue(episodeRepository.count() > 1);
        User user = new User();
        user.setLogin("ainur");
        userRepository.save(user);
        userFilm.setUser(user);
        userFilmRepository.save(userFilm);
        List<Episode> unwatchedList = episodeService.getUnwatchedList(user);

        userFilmRepository.deleteAll();
        episodeRepository.deleteAll();
        filmRepository.deleteAll();
        userRepository.deleteAll();
        assertTrue(unwatchedList.size() != 0);
    }
}
