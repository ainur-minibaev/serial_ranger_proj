package repository;

import config.DataSourceTestConfig;
import config.PersistenceConfig;
import model.Film;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;

import static org.junit.Assert.*;
import static repository.fixture.TestConstants.FilmConstants.FILM_NAME;
import static repository.fixture.TestData.standardFilm;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceTestConfig.class, PersistenceConfig.class})
public class FilmRepositoryTest {

    @Autowired
    private FilmRepository filmRepository;

    @Test
    public void testFindAll() throws SQLException {
        filmRepository.save(standardFilm());
        Iterable<Film> filmList = filmRepository.findAll();
        assertNotNull(filmList);
        assertTrue(filmList.iterator().hasNext());
        for (Film film : filmList) {
            assertNotNull(film);
            assertNotNull(film.getId());
//            assertNotNull(film.getEpisodes());
        }
    }

    @Test
    public void testCRUD() {
        Film film = standardFilm();
        filmRepository.save(film);
        film = filmRepository.findOne(film.getId());
        assertEquals(film.getName(), FILM_NAME);
        film.setName("X-men");
        filmRepository.save(film);
        assertEquals(filmRepository.findOne(film.getId()).getName(), "X-men");
        filmRepository.delete(film);
        assertNull(filmRepository.findOne(film.getId()));
    }
}
