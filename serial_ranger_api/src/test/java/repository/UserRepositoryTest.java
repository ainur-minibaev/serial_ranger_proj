package repository;

import config.DataSourceTestConfig;
import config.PersistenceConfig;
import model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;

import static org.junit.Assert.*;
import static repository.fixture.TestConstants.UserConstants.USER_NAME;
import static repository.fixture.TestData.standardUser;

/**
 * Created by Ольга on 02.06.2014.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceTestConfig.class, PersistenceConfig.class})
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindAll() throws SQLException {
        userRepository.save(standardUser());
        Iterable<User> usersList = userRepository.findAll();
        assertNotNull(usersList);
        assertTrue(usersList.iterator().hasNext());
        for (User user : usersList) {
            assertNotNull(user);
            assertNotNull(user.getId());
        }
    }

    @Test
    public void testCRUD() {
        User user = standardUser();
        userRepository.save(user);
        user = userRepository.findOne(user.getId());
        assertEquals(user.getLogin(), USER_NAME);
        user.setLogin("Ainur");
        userRepository.save(user);
        assertEquals(userRepository.findOne(user.getId()).getLogin(), "Ainur");
        userRepository.delete(user);
        assertNull(userRepository.findOne(user.getId()));
    }
}
