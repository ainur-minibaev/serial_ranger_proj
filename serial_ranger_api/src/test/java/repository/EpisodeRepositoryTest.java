package repository;

import config.DataSourceTestConfig;
import config.PersistenceConfig;
import model.Episode;
import model.Film;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;

import static org.junit.Assert.*;
import static repository.fixture.TestConstants.FilmConstants.FILM_NAME;
import static repository.fixture.TestData.standardEpisode;

/**
 * Created by Ольга on 02.06.2014.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceTestConfig.class, PersistenceConfig.class})
public class EpisodeRepositoryTest {

    @Autowired
    EpisodeRepository episodeRepository;

    @Autowired
    FilmRepository filmRepository;

    private Episode createStandardEpisode() {
        Episode episode = standardEpisode();
        Film film = filmRepository.save(episode.getFilm());
        episode.setFilm(film);
        return episodeRepository.save(episode);
    }

    @Test
    public void testFindAll() throws SQLException {
        createStandardEpisode();
        Iterable<Episode> episodeList = episodeRepository.findAll();
        assertNotNull(episodeList);
        assertTrue(episodeList.iterator().hasNext());
        for (Episode episode : episodeList) {
            assertNotNull(episode);
            assertNotNull(episode.getId());
            assertNotNull(episode.getFilm());
            assertNotNull(episode.getLastEpisode());

        }

    }

    @Test
    public void testCRUD() {
        Episode episode = createStandardEpisode();
        episode = episodeRepository.findOne(episode.getId());
        assertEquals(episode.getFilm().getName(), FILM_NAME);
        episode.setLastEpisode(25);
        episodeRepository.save(episode);
        assertEquals(episodeRepository.findOne(episode.getId()).getLastEpisode(), Integer.valueOf(25));
        episodeRepository.delete(episode);
        assertNull(episodeRepository.findOne(episode.getId()));
    }
}
