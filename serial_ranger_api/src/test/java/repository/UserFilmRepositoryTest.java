package repository;

import config.DataSourceTestConfig;
import config.PersistenceConfig;
import model.Episode;
import model.UserFilm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;

import static org.junit.Assert.*;
import static repository.fixture.TestConstants.FilmConstants.FILM_NAME;
import static repository.fixture.TestData.standardEpisode;
import static repository.fixture.TestData.standardUserFilm;

/**
 * Created by Ольга on 02.06.2014.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceTestConfig.class, PersistenceConfig.class})
public class UserFilmRepositoryTest {

    @Autowired
    UserFilmRepository userFilmRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EpisodeRepository episodeRepository;

    @Autowired
    FilmRepository filmRepository;

    private Episode createStandardEpisode() {
        Episode episode = standardEpisode();
        filmRepository.save(episode.getFilm());

        return episodeRepository.save(episode);
    }

    private UserFilm createStandardUserFilm() {
        UserFilm userFilm = standardUserFilm();
        filmRepository.save(userFilm.getFilm());
        userFilm.getEpisode().setFilm(userFilm.getFilm());
        userRepository.save(userFilm.getUser());
        episodeRepository.save(userFilm.getEpisode());
        return userFilmRepository.save(userFilm);
    }


    @Test
    public void testFindAll() throws SQLException {
        createStandardUserFilm();
        Iterable<UserFilm> userFilmsList = userFilmRepository.findAll();
        assertNotNull(userFilmsList);
        assertTrue(userFilmsList.iterator().hasNext());
        for (UserFilm film : userFilmsList) {
            assertNotNull(film);
            assertNotNull(film.getId());
            assertNotNull(film.getFilm());
            assertNotNull(film.getEpisode());
            assertNotNull(film.getUser());
        }
    }

    @Test
    public void testCRUD() {
        UserFilm userFilm = createStandardUserFilm();
        userFilm = userFilmRepository.findOne(userFilm.getId());
        assertEquals(userFilm.getFilm().getName(), FILM_NAME);
        userFilm.setEpisode(createStandardEpisode());
        userFilmRepository.save(userFilm);
        assertEquals(userFilmRepository.findOne(userFilm.getId()).getEpisode().getLastEpisode(), standardEpisode().getLastEpisode());
        userFilmRepository.delete(userFilm);
        assertNull(userFilmRepository.findOne(userFilm.getId()));
    }
}
