package repository.fixture;

public interface TestConstants {
    interface CVConstants {
        String CV_TITLE = "Software developer";
    }
    interface CompanyConstants {
        String COMPANY_NAME = "HP";
    }
    interface UserConstants {
        String USER_NAME = "Александра";

    }
    interface VacancyConstants {
        String VACANCY_TITLE = "Аналитик отдела продаж";
    }
    interface FilmConstants {
        String FILM_NAME = "Inception";
    }
}
