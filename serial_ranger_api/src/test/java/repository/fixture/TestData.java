package repository.fixture;

import model.Episode;
import model.Film;
import model.User;
import model.UserFilm;

import java.util.ArrayList;
import java.util.List;

import static repository.fixture.TestConstants.FilmConstants.FILM_NAME;
import static repository.fixture.TestConstants.UserConstants.USER_NAME;

public class TestData {

    public static Film standardFilm() {
        Film film = new Film();
        film.setName(FILM_NAME);
//        film.setEpisodes(standardListEpisodes());
        return film;
    }
    public static User standardUser() {
        User user = new User();
        user.setLogin(USER_NAME);
        user.setEmail("alex@gmail.com");
        user.setPassword("qwe");
        return user;
    }

    public static Episode standardEpisode(){
        Episode episode = new Episode();
        episode.setFilm(standardFilm());
        episode.setLastEpisode(12);
        return episode;
    }

    public static UserFilm standardUserFilm(){
         UserFilm userFilm = new UserFilm();
         userFilm.setEpisode(standardEpisode());
        userFilm.setUser(standardUser());
        userFilm.setFilm(standardFilm());
        return userFilm;
    }

    public static List<Episode> standardListEpisodes(){
        List<Episode> episodeList = new ArrayList<Episode>();
        episodeList.add(standardEpisode());
        episodeList.add(standardEpisode());
        episodeList.add(standardEpisode());
        episodeList.add(standardEpisode());
        return episodeList;
    }
}
